﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using OpenBanking.Api.Domain.Account.FairWayBank;

namespace OpenBanking.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FairWayBankController : ControllerBase
    {
	    readonly IGetFairWayBankAccountInformationQuery _getFairWayBankAccountInformationQuery;

	    public FairWayBankController(IGetFairWayBankAccountInformationQuery getFairWayBankAccountInformationQuery)
	    {
		    _getFairWayBankAccountInformationQuery = getFairWayBankAccountInformationQuery;
	    }

	    [HttpGet("{accountNumber}")]
	    public IActionResult Get(string accountNumber)
	    {
		    var result = _getFairWayBankAccountInformationQuery.GetBankAccountInformation(accountNumber);

		    switch (result.Result)
		    {
				case HttpStatusCode.OK:
					return new JsonResult(result.Data);

				case HttpStatusCode.BadRequest:
					return new JsonResult(new {message = "Account Could Not Be Found"});
		    }

		    return BadRequest();
	    }

    }
}