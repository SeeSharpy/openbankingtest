﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpenBanking.Api.Domain.Account.BizfiBank;

namespace OpenBanking.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BizfiBankController : ControllerBase
    {
	    readonly IGetBizfiBankAccountInformationQuery _getBizfiBankAccountInformationQuery;

	    public BizfiBankController(IGetBizfiBankAccountInformationQuery getBizfiBankAccountInformationQuery)
	    {
		    _getBizfiBankAccountInformationQuery = getBizfiBankAccountInformationQuery;
	    }

	    [HttpGet("{accountNumber}")]
	    public IActionResult Get(string accountNumber)
	    {
		    var result = _getBizfiBankAccountInformationQuery.GetBankAccountInformation(accountNumber);

		    switch (result.Result)
		    {
				case HttpStatusCode.OK:
					return new JsonResult(result.Data);

				case HttpStatusCode.BadRequest:
					return new JsonResult(new {message = "Invalid Account Number Format"});

				case HttpStatusCode.NotFound:
					return new JsonResult(new { message = "Account Number Not Found" });
		    }

		    return BadRequest();
	    }

    }
}