﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace OpenBanking.Api.Domain.Account.BizfiBank
{
	public class BizfiBankAccount 
	{
		[JsonProperty]
		public string AccountName { get; set; }
		[JsonProperty]
		public string AccountNumber { get; set; }
		[JsonProperty]
		public string SortCode { get; set; }
		[JsonProperty]
		public double Balance { get; set; }
		[JsonProperty]
		public double AvailableBalance { get; set; }
		[JsonProperty]
		public double Overdraft { get; set; }
		
		[JsonProperty]
		public IEnumerable<Transaction> Transactions { get; set; }
	}

	public class Transaction
	{
		[JsonProperty]
		public double Amount { get; set; }
		[JsonProperty]
		public string Merchant { get; set; }
		[JsonProperty]
		public DateTime ClearedDate { get; set; }
	}
}
