﻿using System;
using System.Collections.Generic;
using System.Net;
using RestSharp;

namespace OpenBanking.Api.Domain.Account.BizfiBank
{
	public class GetBizfiBankAccountInformationQuery : IGetBizfiBankAccountInformationQuery
	{
		const string BaseUrl = "http://bizfibank-bizfitech.azurewebsites.net/api/v1/accounts/";
		readonly IRestClient _restClient;

		public GetBizfiBankAccountInformationQuery(IRestClient restClient)
		{
			_restClient = restClient;
		}

		public HttpFetchResult<HttpStatusCode, BizfiBankAccount> GetBankAccountInformation(string accountNumber)
		{
			_restClient.BaseUrl = new Uri(BaseUrl);


			var response = _restClient.Execute<BizfiBankAccount>(new RestRequest(accountNumber));

			if (response.StatusCode == HttpStatusCode.OK)
			{
				response.Data.Transactions = _restClient.Execute<List<Transaction>>(new RestRequest($"{accountNumber}/transactions")).Data;
			}

			return new HttpFetchResult<HttpStatusCode, BizfiBankAccount>(response.StatusCode, response.Data);
		}
	}
}