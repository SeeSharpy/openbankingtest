﻿using System.Net;

namespace OpenBanking.Api.Domain.Account.BizfiBank
{
	public interface IGetBizfiBankAccountInformationQuery
	{
		HttpFetchResult<HttpStatusCode, BizfiBankAccount> GetBankAccountInformation(string accountNumber);
	}
}