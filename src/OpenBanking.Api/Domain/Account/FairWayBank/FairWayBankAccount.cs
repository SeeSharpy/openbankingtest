﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace OpenBanking.Api.Domain.Account.FairWayBank
{
	public class FairWayBankAccount
	{
		[JsonProperty]
		public string Name { get; set; }

		[JsonProperty]
		public AccountIdentifier Identifier { get; set; }

		[JsonProperty]
		public IEnumerable<Transaction> Transactions { get; set; }
	}

	public class AccountIdentifier
	{
		[JsonProperty]
		public string AccountNumber { get; set; }
		[JsonProperty]
		public string SortCode { get; set; }
	}

	public class Transaction
	{
		[JsonProperty]
		public double Amount { get; set; }
		[JsonProperty]
		public string TransactionInformation { get; set; }
		[JsonProperty]
		public TransactionType Type { get; set; }
		[JsonProperty]
		public DateTime BookedDate { get; set; }
	}

	public enum TransactionType
	{
		Credit = 0,
		Debit = 1
	}
}