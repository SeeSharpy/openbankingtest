﻿using System.Net;

namespace OpenBanking.Api.Domain.Account.FairWayBank
{
	public interface IGetFairWayBankAccountInformationQuery
	{
		HttpFetchResult<HttpStatusCode, FairWayBankAccount> GetBankAccountInformation(string accountNumber);
	}
}