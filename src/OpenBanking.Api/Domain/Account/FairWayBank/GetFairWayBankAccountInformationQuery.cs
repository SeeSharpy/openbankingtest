﻿using System;
using System.Collections.Generic;
using System.Net;
using RestSharp;

namespace OpenBanking.Api.Domain.Account.FairWayBank
{
	public class GetFairWayBankAccountInformationQuery : IGetFairWayBankAccountInformationQuery
	{
		const string BaseUrl = "http://fairwaybank-bizfitech.azurewebsites.net/api/v1/accounts/";
		readonly IRestClient _restClient;

		public GetFairWayBankAccountInformationQuery(IRestClient restClient)
		{
			_restClient = restClient;
		}

		public HttpFetchResult<HttpStatusCode, FairWayBankAccount> GetBankAccountInformation(string accountNumber)
		{
			_restClient.BaseUrl = new Uri(BaseUrl);

			var response = _restClient.Execute<FairWayBankAccount>(new RestRequest(accountNumber));

			if (response.StatusCode == HttpStatusCode.OK)
			{
				response.Data.Transactions = _restClient.Execute<List<Transaction>>(new RestRequest($"{accountNumber}/transactions")).Data;
			}

			return new HttpFetchResult<HttpStatusCode, FairWayBankAccount>(response.StatusCode, response.Data);
		}
	}
}