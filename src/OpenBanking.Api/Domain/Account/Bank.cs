﻿namespace OpenBanking.Api.Domain.Account
{
	public enum Bank
	{
		BizfiBank,
		FairwayBank
	}
}