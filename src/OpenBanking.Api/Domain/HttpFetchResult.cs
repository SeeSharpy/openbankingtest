﻿namespace OpenBanking.Api.Domain
{
	public class HttpFetchResult<HttpStatusCode, T> where T: class
	{
		public HttpFetchResult(HttpStatusCode result, T data)
		{
			Result = result;
			Data = data;
		}

		public HttpStatusCode Result { get; private set; }
		public T Data { get; private set; }
	}
}