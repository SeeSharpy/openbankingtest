﻿using System.Net;
using FluentAssertions;
using NSubstitute;
using OpenBanking.Api.Domain;
using OpenBanking.Api.Domain.Account.FairWayBank;
using RestSharp;
using Xunit;

namespace OpenBanking.Api.Tests.Domain.Account.FairWayBank
{
	public class WhenQueryingWithAnInvalidAccountNumber
	{
		const string AccountNumber = "NOTVALID";

		readonly HttpFetchResult<HttpStatusCode, FairWayBankAccount> _result;

		public WhenQueryingWithAnInvalidAccountNumber()
		{
			var restClient = Substitute.For<IRestClient>();

			restClient.Execute<FairWayBankAccount>(Arg.Any<IRestRequest>())
				.Returns(new RestResponse<FairWayBankAccount>
				{
					StatusCode = HttpStatusCode.BadRequest,
					Data = null
				});

			var subject = new GetFairWayBankAccountInformationQuery(restClient);

			_result = subject.GetBankAccountInformation(AccountNumber);
		}

		[Fact]
		public void ResponseShouldBeHttpBadRequest()
		{
			_result.Result.Should().Be(HttpStatusCode.BadRequest);
		}

		[Fact]
		public void ReturnedAccountNumberShouldBeTheSame()
		{
			_result.Data.Should().BeNull();
		}
	}
}