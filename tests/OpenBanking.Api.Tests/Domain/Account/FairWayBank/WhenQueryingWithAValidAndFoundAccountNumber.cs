﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentAssertions;
using NSubstitute;
using OpenBanking.Api.Domain;
using OpenBanking.Api.Domain.Account.FairWayBank;
using RestSharp;
using Xunit;

namespace OpenBanking.Api.Tests.Domain.Account.FairWayBank
{
	public class WhenQueryingWithAValidAndFoundAccountNumber
	{
		const string AccountNumber = "12345678";

		readonly HttpFetchResult<HttpStatusCode, FairWayBankAccount> _result;

		public WhenQueryingWithAValidAndFoundAccountNumber()
		{
			var restClient = Substitute.For<IRestClient>();

			restClient.Execute<FairWayBankAccount>(Arg.Is<RestRequest>(x => x.Resource == AccountNumber))
			.Returns(new RestResponse<FairWayBankAccount>
			{
				StatusCode = HttpStatusCode.OK,
				Data = new FairWayBankAccount
				{
					Name = "Test Name",
					Identifier = new AccountIdentifier
					{
						AccountNumber = AccountNumber,
						SortCode = "00-00-00"
					}
				}
			});

			restClient.Execute<List<Transaction>>(Arg.Is<RestRequest>(x => x.Resource == $"{AccountNumber}/transactions"))
				.Returns(new RestResponse<List<Transaction>>
				{
					Data = new List<Transaction>
					{
						new Transaction
						{
							Amount = -100,
							BookedDate = DateTime.Now,
							TransactionInformation = "Test Merchant",
							Type = TransactionType.Credit
						}
					}
				});

			var subject = new GetFairWayBankAccountInformationQuery(restClient);

			_result = subject.GetBankAccountInformation(AccountNumber);
		}

		[Fact]
		public void ResponseShouldBeHttpOk()
		{
			_result.Result.Should().Be(HttpStatusCode.OK);
		}

		[Fact]
		public void ReturnedAccountNumberShouldBeTheSame()
		{
			_result.Data.Identifier.AccountNumber.Should().Be(AccountNumber);
		}

		[Fact]
		public void ReturnedAccountShouldHaveTransactions()
		{
			_result.Data.Transactions.Count().Should().BeGreaterThan(0);
		}
	}
}
