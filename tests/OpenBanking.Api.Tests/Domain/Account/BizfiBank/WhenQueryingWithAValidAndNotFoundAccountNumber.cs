﻿using System.Collections.Generic;
using System.Net;
using FluentAssertions;
using NSubstitute;
using OpenBanking.Api.Domain;
using OpenBanking.Api.Domain.Account.BizfiBank;
using RestSharp;
using Xunit;

namespace OpenBanking.Api.Tests.Domain.Account.BizfiBank
{
	public class WhenQueryingWithAValidAndNotFoundAccountNumber
	{
		const string AccountNumber = "12345678";

		readonly HttpFetchResult<HttpStatusCode, BizfiBankAccount> _result;

		public WhenQueryingWithAValidAndNotFoundAccountNumber()
		{
			var restClient = Substitute.For<IRestClient>();

			restClient.Execute<BizfiBankAccount>(Arg.Is<RestRequest>(x => x.Resource == AccountNumber))
				.Returns(new RestResponse<BizfiBankAccount>
				{
					StatusCode = HttpStatusCode.NotFound,
					Data = null
				});

			var subject = new GetBizfiBankAccountInformationQuery(restClient);

			_result = subject.GetBankAccountInformation(AccountNumber);
		}

		[Fact]
		public void ResponseShouldBeHttpNotFound()
		{
			_result.Result.Should().Be(HttpStatusCode.NotFound);
		}

		[Fact]
		public void ReturnedAccountNumberShouldBeTheSame()
		{
			_result.Data.Should().BeNull();
		}
	}
}