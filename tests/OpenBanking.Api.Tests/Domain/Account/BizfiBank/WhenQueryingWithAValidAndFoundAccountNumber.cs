﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentAssertions;
using NSubstitute;
using OpenBanking.Api.Domain;
using OpenBanking.Api.Domain.Account.BizfiBank;
using RestSharp;
using Xunit;

namespace OpenBanking.Api.Tests.Domain.Account.BizfiBank
{
	public class WhenQueryingWithAValidAndFoundAccountNumber
	{
		const string AccountNumber = "12345678";

		readonly HttpFetchResult<HttpStatusCode, BizfiBankAccount> _result;

		public WhenQueryingWithAValidAndFoundAccountNumber()
		{
			var restClient = Substitute.For<IRestClient>();

			restClient.Execute<BizfiBankAccount>(Arg.Is<RestRequest>(x=> x.Resource == AccountNumber))
			.Returns(new RestResponse<BizfiBankAccount>
			{
				StatusCode = HttpStatusCode.OK,
				Data = new BizfiBankAccount
				{
					AccountNumber = AccountNumber,
					AccountName = "Test Name",
					AvailableBalance = 1.00,
					Balance = 2.00,
					Overdraft = 2.00,
					SortCode = "00-00-00"
				}
			});

			restClient.Execute<List<Transaction>>(Arg.Is<RestRequest>(x=> x.Resource == $"{AccountNumber}/transactions"))
				.Returns(new RestResponse<List<Transaction>>
				{
					Data = new List<Transaction>
					{
						new Transaction
						{
							ClearedDate = new DateTime(2019, 1, 1, 0, 0, 0),
							Merchant = "Test Merchant",
							Amount = -1.00
						}
					}
				});


			var subject = new GetBizfiBankAccountInformationQuery(restClient);

			_result = subject.GetBankAccountInformation(AccountNumber);
		}

		[Fact]
		public void ResponseShouldBeHttpOk()
		{
			_result.Result.Should().Be(HttpStatusCode.OK);
		}

		[Fact]
		public void ReturnedAccountNumberShouldBeTheSame()
		{
			_result.Data.AccountNumber.Should().Be(AccountNumber);
		}

		[Fact]
		public void ReturnedAccountShouldHaveTransactions()
		{
			_result.Data.Transactions.Count().Should().BeGreaterThan(0);
		}
	}
}
