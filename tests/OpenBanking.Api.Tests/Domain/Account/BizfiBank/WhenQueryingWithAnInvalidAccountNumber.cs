﻿using System;
using System.Collections.Generic;
using System.Net;
using FluentAssertions;
using NSubstitute;
using OpenBanking.Api.Domain;
using OpenBanking.Api.Domain.Account.BizfiBank;
using RestSharp;
using Xunit;

namespace OpenBanking.Api.Tests.Domain.Account.BizfiBank
{
	public class WhenQueryingWithAnInvalidAccountNumber
	{
		const string AccountNumber = "NOTVALID";

		readonly HttpFetchResult<HttpStatusCode, BizfiBankAccount> _result;

		public WhenQueryingWithAnInvalidAccountNumber()
		{
			var restClient = Substitute.For<IRestClient>();

			restClient.Execute<BizfiBankAccount>(Arg.Is<RestRequest>(x => x.Resource == AccountNumber))
				.Returns(new RestResponse<BizfiBankAccount>
				{
					StatusCode = HttpStatusCode.BadRequest,
					Data = null
				});

			var subject = new GetBizfiBankAccountInformationQuery(restClient);

			_result = subject.GetBankAccountInformation(AccountNumber);
		}

		[Fact]
		public void ResponseShouldBeHttpBadRequest()
		{
			_result.Result.Should().Be(HttpStatusCode.BadRequest);
		}

		[Fact]
		public void ReturnedAccountNumberShouldBeTheSame()
		{
			_result.Data.Should().BeNull();
		}
	}
}